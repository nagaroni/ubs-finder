# frozen_string_literal: true

namespace :ubs_data do
  desc 'Import ubs data from csv file'
  task import: [:environment] do
    require 'csv'

    ubs_file_path = Rails.root.join('db', 'fixtures', 'ubs.csv')
    levels = {
      'Desempenho muito acima da média' => 1,
      'Desempenho acima da média' => 2,
      'Desempenho mediano ou  um pouco abaixo da média' => 3
    }
    normalize_level = lambda do |str|
      levels.fetch(str)
    end

    total_imported = 0
    result = Benchmark.realtime do
      data = CSV.open(ubs_file_path, headers: :first_row).map do |row|
        {
          geocode:  {
            lat: row['vlr_latitude'].to_f,
            long: row['vlr_longitude'].to_f
          },
          address: row.values_at('dsc_endereco', 'dsc_bairro').compact.join(', '),
          scores: {
            size: normalize_level.call(row['dsc_estrut_fisic_ambiencia']),
            adaptation_for_seniors: normalize_level.call(row['dsc_adap_defic_fisic_idosos']),
            medical_equipement: normalize_level.call(row['dsc_equipamentos']),
            medicine: normalize_level.call(row['dsc_medicamentos'])
          },
          name: row['nom_estab'],
          phone: row['dsc_telefone'],
          cnes: row['cod_cnes'],
          city: row['dsc_cidade']
        }
      end

      ApplicationRecord.transaction do
        Ubs.create!(data)
      end

      total_imported = data.size
    end

    puts "Imported #{total_imported} UBS. Time elapsed: #{result}"
  end
end
