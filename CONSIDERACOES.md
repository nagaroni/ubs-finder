# Solução proposta

- Adicionar classe responsável por aplicar o filtro de acordo com o parâmetro recebido.
- Adicionar Representers para dispors o resultado no padrão proposto

# Critérios Atendidos
 - busca retorna somente 10 ubs na resultado por padrão
 - busca permite filtro por query(gecode), per_page(quantidade por pagina), page ( pagina escolhida )
 - formato esperado do retorno

# Pontos de Melhorias
 - Cenários de testes, adicionar teste para cobrir efeitos colaterais e/ou falhas.
 - Adicionar indice para ubs.geocode para melhorar a busca por query
 - Adicionar Doc da api
 - Adicionar _links no retorno da API
 - Paginação
 - Adicionar teste para rake ubs_data:import
 - Performar o import feito pela rake ubs_data:import
