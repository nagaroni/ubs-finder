FROM ruby:2.4.1

RUN apt-get update -qq && apt-get install -y --force-yes vim postgresql-client
RUN git config --global user.name "ubsfinder"
RUN git config --global user.email "ubsfinder@bionexo.com.br"

WORKDIR /tmp
ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
RUN bundle install

WORKDIR /ubsfinder
ADD . /ubsfinder

CMD ["rails", "server", "-b" "0.0.0.0"]
