# Sobre

O `ubs_finder` é uma aplicação feita com ruby e rails com o objetivo de permitir
que outras aplicações possam consultar os microdados sobre as UBS (Unidade Básica de Sáude),
para isso a aplicação disponibiliza uma rota que permite fazer consultas com filtros.

## Dependências
Este projeto faz uso do [docker]() e [docker-compose](), caso não tenha
instalado você pode fazer o download nos links acima ou instalar a seguintes
depencias em sua maquina:

 - ruby ~> 2.4.1
 - bundler ~> 1.16.0
 - postgres ~> 9.6

### Instalação
Para instalar as dependências, configurar o banco de dados e etc:

```sh
$ bin/setup
```

## Docker

```
# setup database
docker-compose run --rm api bundle exec rake db:create db:migrate

# start app server
docker-compose up api

# rails console
docker-compose run --rm api bin/rails c

# bash prompt
docker-compose run --rm api /bin/bash

# importar ubs data
docker-compose run --rm api bundle exec rake ubs_data:import

```

## Tests

Para rodar os testes começe um console bash e rode o comando:
`rspec`
