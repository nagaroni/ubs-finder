# frozen_string_literal: true

class UbsSearcher
  def initialize(query: nil, per_page: nil, page: nil)
    @query = query
    @per_page = per_page.to_i
    @page = page.to_i
  end

  def search
    apply_filter(&start_from)
    apply_filter(&limit_result)
    apply_filter(&search_by_geocode) if query?

    ubs_collection
  end

  def page
    return DEFAULT_PAGE if @page < 1

    @page
  end

  def result_per_page
    return DEFAULT_LIMIT if @per_page < 1

    @per_page
  end

  private

  DEFAULT_LIMIT = 10
  DEFAULT_PAGE = 1
  private_constant :DEFAULT_LIMIT, :DEFAULT_PAGE

  def search_by_geocode
    -> { Ubs.by_geocode(*@query.split(',')) }
  end

  def limit_result
    -> { Ubs.limit(result_per_page) }
  end

  def start_from
    -> { Ubs.offset(normalized_offset) }
  end

  def apply_filter(&block)
    @ubs_collection = ubs_collection.scoping(&block)
  end

  def ubs_collection
    @ubs_collection ||= Ubs.all.order(:id)
  end

  def normalized_offset
    (page * result_per_page) - result_per_page
  end

  def query?
    @query.present?
  end
end
