# frozen_string_literal: true

class Ubs < ApplicationRecord
  scope :by_geocode, (lambda do |lat, lng|
    where('geocode->>\'lat\' = ? and geocode->>\'long\' = ?', lat, lng)
  end)
end
