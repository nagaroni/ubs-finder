# frozen_string_literal: true

module V1
  class UbsController < ApplicationController
    def index
      render json: search_result
    end

    private

    def search_result
      ubs_seacher = UbsSearcher.new(**search_params)
      entries = ubs_seacher.search

      UbsSearchRepresenter.new(
        current_page: ubs_seacher.page,
        per_page: ubs_seacher.result_per_page,
        entries: entries
      )
    end

    def search_params
      params.permit(:query, :per_page, :page).to_h.symbolize_keys
    end
  end
end
