# frozen_string_literal: true

class UbsSearchRepresenter
  attr_reader :current_page, :per_page, :entries

  def initialize(current_page:, per_page:, entries:)
    @current_page = current_page
    @per_page = per_page
    @entries = entries
  end

  def total_entries
    @total_entries ||= Ubs.count
  end

  def as_json(*)
    mapped_entries = entries.map do |entry|
      UbsRepresenter.new(entry).as_json
    end

    {
      'current_page' => current_page,
      'per_page' => per_page,
      'total_entries' => total_entries,
      'entries' => mapped_entries
    }
  end
end
