# frozen_string_literal: true

class UbsRepresenter < SimpleDelegator
  def as_json(*)
    { 'id' => cnes }.merge(
      super.except('id', 'cnes', 'created_at', 'updated_at')
    )
  end
end
