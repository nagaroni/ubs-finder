class CreateUbs < ActiveRecord::Migration[5.2]
  def change
    create_table :ubs do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :phone
      t.integer :cnes, index: { unique: true }
      t.jsonb :geocode
      t.jsonb :scores
      t.timestamps
    end
  end
end
