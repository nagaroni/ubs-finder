# frozen_string_literal: true

Rails.application.routes.draw do
  scope :api do
    namespace :v1 do
      get '/find_ubs', to: 'ubs#index'
    end
  end
end
