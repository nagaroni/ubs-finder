# frozen_string_literal: true

require 'rails_helper'

describe 'Endpoint to query for UBS', type: :request do
  context 'successfully' do
    describe 'GET /api/v1/find_ubs' do
      it 'returns current_page, per_page, total_entries and entries on body' do
        get '/api/v1/find_ubs'

        expected_keys = %w[current_page per_page total_entries entries]
        json_response = JSON.parse(response.body)
        keys_from_response = json_response.keys

        expect(keys_from_response).to match(expected_keys)
      end

      it 'return ubs entries without updated_at and created_at' do
        create(:ubs)
        get '/api/v1/find_ubs'
        json_response = JSON.parse(response.body)
        entry = json_response['entries'].first

        expect(entry).not_to have_key('updated_at')
        expect(entry).not_to have_key('created_at')
      end

      context 'when request has no query parameters' do
        it 'returns ubs 10 entries by default' do
          create_list(:ubs, 30)
          get '/api/v1/find_ubs'

          json_response = JSON.parse(response.body)
          expected_ubs = Ubs.first(10)
          entries = json_response['entries']

          expect(json_response['current_page']).to eq(1)
          expect(json_response['total_entries']).to eq(Ubs.count)
          expect(json_response['per_page']).to eq(10)
          expect_entries_to_match_ubs_collection(entries, expected_ubs)
        end
      end

      context 'when requested with query' do
        it 'returns ubs by its geocode' do
          create_list(:ubs, 10)
          geocode = {
            lat: FFaker::Geolocation.lat, long: FFaker::Geolocation.lng
          }
          expected_result = create_list(:ubs, 10, geocode: geocode)
          params = { query: geocode.values.join(',') }
          get '/api/v1/find_ubs', params: params

          json_response = JSON.parse(response.body)
          entries = json_response['entries']

          expect_entries_to_match_ubs_collection(entries, expected_result)
        end
      end

      context 'when requested with geocode and per_page' do
        it 'returns ubs by its geocode with limited result by per_page' do
          create_list(:ubs, 10)
          geocode = {
            lat: FFaker::Geolocation.lat, long: FFaker::Geolocation.lng
          }
          ubs_collection = create_list(:ubs, 10, geocode: geocode)
          expected_result = ubs_collection.first(5)
          per_page = 5
          params = {
            query: geocode.values.join(','), per_page: per_page
          }
          get '/api/v1/find_ubs', params: params

          json_response = JSON.parse(response.body)
          entries = json_response['entries']

          expect(json_response['per_page']).to eq(per_page)
          expect(entries.size).to eq(per_page)
          expect_entries_to_match_ubs_collection(entries, expected_result)
        end
      end

      context 'when requested with page and per_page' do
        it 'returns paginated ubs with limited result by per_page' do
          ubs_colletion = create_list(:ubs, 100)
          per_page = 3
          page = 17
          params = { per_page: per_page, page: page }

          get '/api/v1/find_ubs', params: params
          json_response = JSON.parse(response.body)
          entries = json_response['entries']

          expect(json_response['per_page']).to eq(per_page)
          expect(json_response['current_page']).to eq(page)
          expect(entries.size).to eq(per_page)
          expect_entries_to_match_ubs_collection(entries, ubs_colletion[48..51])
        end
      end

      context 'when requested  with page' do
        it 'returns paginated ubs' do
          ubs_colletion = create_list(:ubs, 40)
          page = 4
          params = { page: page }

          get '/api/v1/find_ubs', params: params
          json_response = JSON.parse(response.body)
          entries = json_response['entries']

          expect(json_response['current_page']).to eq(page)
          expect(entries.size).to eq(10)
          expect_entries_to_match_ubs_collection(
            entries, ubs_colletion.last(10)
          )
        end

        it 'returns empty when page is greater than total of pages' do
          create_list(:ubs, 20)
          page = 3
          params = { page: page }

          get '/api/v1/find_ubs', params: params
          json_response = JSON.parse(response.body)
          entries = json_response['entries']

          expect(entries).to eq([])
        end
      end

      context 'when requested with query, per_page and page' do
        it 'returns ubs filtered by query, per_age and page' do
          geocode = {
            lat: FFaker::Geolocation.lat, long: FFaker::Geolocation.lng
          }
          create_list(:ubs, 99)
          create_list(:ubs, 15, geocode: geocode)
          page = 4
          per_page = 4
          query = geocode.values.join(',')
          params = { page: page, per_page: per_page, query: query }

          get '/api/v1/find_ubs', params: params
          json_response = JSON.parse(response.body)
          entries = json_response['entries']
          size_of_last_page = (15 / 4).ceil
          expected_ubs = Ubs.last(size_of_last_page)

          expect(json_response['current_page']).to eq(page)
          expect(entries.size).to eq(size_of_last_page)
          expect_entries_to_match_ubs_collection(
            entries, expected_ubs
          )
        end
      end
    end

    private

    # rubocop:disable Metrics/AbcSize
    def expect_entries_to_match_ubs_collection(entries, ubs_collection)
      entries.zip(ubs_collection).each do |ubs_entry, expected_ubs|
        expect(ubs_entry['id']).to eq(expected_ubs.cnes)
        expect(ubs_entry['name']).to eq(expected_ubs.name)
        expect(ubs_entry['address']).to eq(expected_ubs.address)
        expect(ubs_entry['city']).to eq(expected_ubs.city)
        expect(ubs_entry['phone']).to eq(expected_ubs.phone)
        expect(ubs_entry['geocode']['lat']).to eq(expected_ubs.geocode['lat'])
        expect(ubs_entry['geocode']['long']).to eq(expected_ubs.geocode['long'])
        expect(ubs_entry['scores']['size']).to eq(expected_ubs.scores['size'])
        expect(ubs_entry['scores']['adaptation_for_seniors'])
          .to eq(expected_ubs.scores['adaptation_for_seniors'])
        expect(ubs_entry['scores']['medical_equipment'])
          .to eq(expected_ubs.scores['medical_equipment'])
        expect(ubs_entry['scores']['medicine'])
          .to eq(expected_ubs.scores['medicine'])
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
