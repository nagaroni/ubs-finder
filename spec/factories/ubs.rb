# frozen_string_literal: true

FactoryBot.define do
  factory(:ubs) do
    name { "UBS #{FFaker::NameBR.name}" }
    address { FFaker::AddressBR.street }
    city { FFaker::AddressBR.city }
    phone { FFaker::PhoneNumberBR.phone_number }
    cnes { SecureRandom.random_number(1_000_000) }
    geocode do
      {
        lat: FFaker::Geolocation.lat,
        long: FFaker::Geolocation.lng
      }
    end
    scores do
      {
        size: rand(0..2),
        adaptation_for_senior: rand(0..2),
        medical_equipment: rand(0..2),
        medicine: rand(0..2)
      }
    end
  end
end
