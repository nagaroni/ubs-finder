# frozen_string_literal: true

require 'rails_helper'

describe UbsSearchRepresenter do
  describe '#total_entries' do
    it 'returns total of ubs registered' do
      ubs_collection = create_list(:ubs, 14)
      result = described_class.new(
        current_page: 1, per_page: 10, entries: ubs_collection
      ).total_entries

      expect(result).to eq(ubs_collection.size)
    end
  end

  describe '#as_json' do
    it 'returns represented ubs search' do
      ubs_collection = create_list(:ubs, 1)
      result = described_class.new(
        current_page: 1, per_page: 10, entries: ubs_collection
      ).as_json

      expect(result['total_entries']).to be_a(Integer)
      expect(result['current_page']).to be_a(Integer)
      expect(result['per_page']).to be_a(Integer)
      expect(result['entries']).to match(Array[Hash])
    end
  end
end
