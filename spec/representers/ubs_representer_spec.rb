# frozen_string_literal: true

require 'rails_helper'

describe UbsRepresenter do
  describe '#as_json' do
    it 'returns hash with represented ubs' do
      ubs = build(:ubs)
      result = described_class.new(ubs).as_json

      expect(result['id']).to be_a(Integer)
      expect(result['name']).to be_a(String)
      expect(result['address']).to be_a(String)
      expect(result['city']).to be_a(String)
      expect(result['geocode']).to be_a(Hash)
      expect(result['geocode']['lat']).to be_a(Float)
      expect(result['geocode']['long']).to be_a(Float)
      expect(result['scores']['size']).to be_a(Integer)
      expect(result['scores']['adaptation_for_senior']).to be_a(Integer)
      expect(result['scores']['medical_equipment']).to be_a(Integer)
      expect(result['scores']['medicine']).to be_a(Integer)
    end
  end
end
