# frozen_string_literal: true

require 'rails_helper'

describe UbsSearcher do
  describe '#search' do
    context 'when query, per_page and page is nil' do
      it 'returns all registered ubs' do
        ubs_collection = create_list(:ubs, 5)
        result = described_class.new.search

        expect(result.size).to eq(5)
        expect(ubs_collection).to match_array(result)
      end

      it 'limits result to only 10 record by default' do
        ubs_collection = create_list(:ubs, 15)
        result = described_class.new.search
        expected_collection = ubs_collection.first(10)
        last_5_ubs = ubs_collection.last(5)

        expect(result.size).to eq(10)
        expect(result).to match_array(expected_collection)
        expect(result).not_to include(last_5_ubs)
      end
    end

    context 'when query is not nil' do
      it 'returns registered ubs filtered by geocode' do
        expected_ubs = create(:ubs)
        create_list(:ubs, 10)
        query = expected_ubs.geocode.values.join(',')
        result = described_class.new(query: query).search

        expect(result.size).to eq(1)
        expect(result).to eq([expected_ubs])
      end

      it 'returns default search when query is an empty string' do
        expected_ubs = create_list(:ubs, 10)
        result = described_class.new(query: '').search

        expect(result.to_a).to match_array(expected_ubs)
      end
    end

    context 'when per_page is not nil' do
      it 'returns registered ubs limit by per_page' do
        ubs_collection = create_list(:ubs, 30)
        per_page = 15
        expected_collection = ubs_collection.first(per_page)
        result = described_class.new(per_page: per_page).search

        expect(result.size).to eq(per_page)
        expect(result.to_a).to match_array(expected_collection)
      end

      it 'returns ubs limited by default limit when per_page is string' do
        ubs_collection = create_list(:ubs, 30)
        expected_collection = ubs_collection.first(10)
        result = described_class.new(per_page: '').search

        expect(result.size).to eq(10)
        expect(result).to match_array(expected_collection)
      end
    end

    context 'when page is not nil' do
      it 'returns registered ubs starting by page 2' do
        ubs_collection = create_list(:ubs, 30)
        expected_collection = ubs_collection[10..19]
        page = 2
        result = described_class.new(page: page).search

        expect(result.to_a).to match_array(expected_collection)
      end

      it 'returns registered ubs starting by page 1' do
        ubs_collection = create_list(:ubs, 30)
        expected_collection = ubs_collection.first(10)
        page = 1
        result = described_class.new(page: page).search

        expect(result.to_a).to match_array(expected_collection)
      end

      it 'returns registered ubs starting by page 5' do
        ubs_collection = create_list(:ubs, 50)
        expected_collection = ubs_collection[40..49]
        page = 5
        result = described_class.new(page: page).search

        expect(result.to_a).to match_array(expected_collection)
      end

      it 'returns default offset when page is less than 1' do
        ubs_collection = create_list(:ubs, 30)
        expected_collection = ubs_collection.first(10)
        result_when_page_is_zero = described_class.new(page: 0).search
        result_when_page_is_negative = described_class.new(page: -3).search

        expect(result_when_page_is_zero.to_a)
          .to match_array(expected_collection)
        expect(result_when_page_is_negative.to_a)
          .to match_array(expected_collection)
      end

      it 'returns default offeset when page is a string' do
        ubs_collection = create_list(:ubs, 30)
        expected_collection = ubs_collection.first(10)
        result_when_page_is_string = described_class.new(page: 'iaa').search

        expect(result_when_page_is_string).to match_array(expected_collection)
      end
    end
  end
end
